import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;

public class Lession2 {

  /**
   * Viết chương trình tính ra số ngày giữa 2 mốc thời điểm người dùng nhập vào. Dữ liệu nhập vào
   * của người dùng sẽ có dạng y1 m1 d1 y2 m2 d2. Với điều kiện mốc thời gian 1 lớn hơn mốc thời
   * gian 2.
   */
  public static void main(String[] args) {
    try {
      int y1 = Integer.parseInt(args[0]);
      int m1 = Integer.parseInt(args[1]);
      int d1 = Integer.parseInt(args[2]);

      int y2 = Integer.parseInt(args[3]);
      int m2 = Integer.parseInt(args[4]);
      int d2 = Integer.parseInt(args[5]);

      LocalDate date1 = LocalDate.of(y1, m1, d1);
      LocalDate date2 = LocalDate.of(y2, m2, d2);

      if (date2.isAfter(date1)) {
        System.out.println("Invalid request!");
        return;
      }

      long noOfDaysBetween = DAYS.between(date2, date1);
      System.out.println(noOfDaysBetween);
    } catch (Exception e) {
      System.out.println("Invalid request!");
    }
  }
}