public class Lession1 {

  enum Month {
    JANUARY(31),
    FEBRUARY(28),
    MARCH(31),
    APRIL(30),
    MAY(31),
    JUNE(30),
    JULY(31),
    AUGUST(31),
    SEPTEMBER(30),
    OCTOBER(31),
    NOVEMBER(30),
    DECEMBER(31);

    private final int days;

    Month(int days) {
      this.days = days;
    }

    public static Month nameOf(String name) {
      for (Month month : Month.values()) {
        if (month.name().equals(name)) {
          return month;
        }
      }
      return null;
    }

    public int getDays() {
      return days;
    }
  }

  public static int getDaysWithMethod1(String arg) {
    Month month = Month.nameOf(arg);
    if (month != null) {
      return month.getDays();
    }
    return -1;
  }

  public static int getDaysWithMethod2(String arg) {
    switch (arg) {
      case "JANUARY":
      case "MARCH":
      case "MAY":
      case "JULY":
      case "AUGUST":
      case "OCTOBER":
      case "DECEMBER":
        return 31;
      case "APRIL":
      case "JUNE":
      case "SEPTEMBER":
      case "NOVEMBER":
        return 30;
      case "FEBRUARY":
        return 28;
      default:
        return -1;
    }
  }

  /**
   * Viết một chương trình nhận vào 1 trong 12 tháng làm input. Output là số ngày trong tháng từ
   * input. Giả sử tháng 2 luôn chỉ có 28 ngày. Yêu cầu làm theo 2 cách: Cách 1: Sử dụng enum và cấu
   * trúc switch. Cách 2: Không sử dụng enum.
   */
  public static void main(String[] args) {
    // Cách 1
    System.out.println("Cách 1: ");
    for (String arg : args) {
      int days = getDaysWithMethod1(arg.toUpperCase());
      if (days != -1) {
        System.out.println(days);
      } else {
        System.out.println("UNKNOWN");
      }
    }
    System.out.println();

    // Cách 2
    System.out.println("Cách 2: ");
    for (String arg : args) {
      int days = getDaysWithMethod2(arg.toUpperCase());
      if (days != -1) {
        System.out.println(days);
      } else {
        System.out.println("UNKNOWN");
      }
    }
  }
}