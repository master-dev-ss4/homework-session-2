public class Lession3 {

  /**
   * Viết một chương trình tính tổng các số nguyên tố từ 1 đến 10_000 với 3 cách. Mỗi cách yêu cầu
   * sử dụng 1 cấu trúc lặp khác nhau bao gồm: while, do while và for.
   */
  public static void main(String[] args) {
    System.out.println(sumWithFor());
    System.out.println(sumWithWhile());
    System.out.println(sumWithDoWhile());
  }

  public static boolean isPrimeNumber(int number) {
    if (number < 2) {
      return false;
    }
    if (number == 2) {
      return true;
    }
    if (number % 2 == 0) {
      return false;
    }
    double maxValue = Math.sqrt(number);
    for (int i = 3; i <= maxValue; i += 2) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }

  public static int sumWithFor() {
    int sum = 2;
    for (int i = 3; i <= 10_000; i += 2) {
      sum += isPrimeNumber(i) ? i : 0;
    }
    return sum;
  }

  public static int sumWithWhile() {
    int sum = 2;
    int i = 3;
    while (i <= 10_000) {
      sum += isPrimeNumber(i) ? i : 0;
      i += 2;
    }
    return sum;
  }

  public static int sumWithDoWhile() {
    int sum = 2;
    int i = 3;
    do {
      sum += isPrimeNumber(i) ? i : 0;
      i += 2;
    } while (i <= 10_000);
    return sum;
  }
}